# frozen_string_literal: true

require 'dynamoid'
require 'telegram/bot'
require 'yaml'
require 'geocoder'

Dir['app/initializers/*.rb'].each { |file| require_relative file }
Dir['app/models/*.rb'].each { |file| require_relative file }
require_relative 'app/data_handler'
