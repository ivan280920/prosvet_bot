# frozen_string_literal: true

class MessageHandler
  attr_reader :message, :api

  def initialize(message, api)
    @message = message
    @api = api
  end

  def call
    handling_variant = handling_variants.find { |variant| variant[:condition].call }
    return unless handling_variant

    user.touch(:used_bot_at)
    method(handling_variant[:method]).call
  end

  private

  def handling_variants
    [
      { condition: -> { simple_replies.keys.include?(message.text) }, method: :handle_simple_reply },
      { condition: -> { want_help_button? }, method: :handle_want_help_button },
      { condition: -> { message.text == texts['buttons']['select_city'] }, method: :handle_select_city_button },
      { condition: -> { texts['buttons']['regions'].include?(message.text) }, method: :handle_region_reply },
      { condition: -> { message.location }, method: :handle_location },
      { condition: -> { message.document && %r{/add_issue}.match?(message.caption) }, method: :handle_document }
    ]
  end

  def simple_replies
    {
      '/start' => { text_key: 'start', button_keys: %w[want_help advice contact report_map] },
      texts['buttons']['menu'] => { text_key: 'menu', button_keys: %w[want_help advice contact report_map] },
      texts['buttons']['advice'] => { text_key: 'advice', button_keys: %w[what_next menu] },
      texts['buttons']['report_map'] => { text_key: 'map', button_keys: %w[want_help menu] },
      texts['buttons']['map'] => { text_key: 'map', button_keys: %w[menu] },
      texts['buttons']['contact'] => { text_key: 'contact', button_keys: %w[want_help menu] },
      texts['buttons']['what_next'] => { text_key: 'groups', button_keys: %w[group_1 group_2 group_3] },
      texts['buttons']['group_1'] => { text_key: 'group_1', button_keys: %w[actual menu] },
      texts['buttons']['group_2'] => { text_key: 'group_2', button_keys: %w[actual menu] },
      texts['buttons']['group_3'] => { text_key: 'group_3', button_keys: %w[actual menu] }
    }
  end

  def handle_simple_reply
    menu_params = simple_replies[message.text].fetch_values(:text_key, :button_keys)

    show_menu(*menu_params)
  end

  def handle_want_help_button
    text = texts['texts']['location']
    keyboard = [
      [{ text: texts['buttons']['send_location'], request_location: true }],
      [texts['buttons']['select_city']],
      [texts['buttons']['menu']]
    ]

    show_reply_keyboard(text, keyboard)
  end

  def handle_select_city_button
    text = texts['texts']['select_city']
    buttons = texts['buttons']['regions'] + [texts['buttons']['menu']]
    keyboard = buttons.each_slice(2).to_a

    show_reply_keyboard(text, keyboard)
  end

  def handle_region_reply
    send_newspapers(message.text)
  end

  def handle_location
    coordinates = [message.location.latitude, message.location.longitude]
    data = Geocoder.search(coordinates, language: :ru)[0].data
    address = data['address']

    if address && address['country'] == 'Беларусь'
      region = (address['state'] || address['city']).split(' ')[0]
      send_newspapers(region)
    else
      send_text_message(texts['errors']['wrong_location'])
    end
  end

  def handle_document
    issue = create_issue

    text = if issue
             msg_vars = { title: issue.newspaper.title, number: issue.number, date: issue.date, version: issue.version }
             texts['texts']['issue_added'] % msg_vars
           else
             texts['errors']['newspaper_not_found']
           end

    send_text_message(text)
  end

  def want_help_button?
    [texts['buttons']['want_help'], texts['buttons']['actual']].include?(message.text)
  end

  def send_newspapers(region)
    data = Newspaper.data_for_region(region)
    user.update_attributes(region: region)

    return send_text_message(texts['errors']['no_newspapers']) if data.empty?

    send_text_message(texts['texts']['newspapers'])

    data.map.with_index do |entry, i|
      caption_vars = { title: entry[:newspaper].title, description: entry[:newspaper].description }
      params = { document: entry[:issue].telegram_file_id, caption: texts['texts']['document_caption'] % caption_vars }
      params.merge!(reply_markup: build_reply_keyboard(%w[map menu])) if i + 1 == data.length

      send_document(params)
    end
  end

  def show_menu(text_key, button_keys)
    show_reply_keyboard(texts['texts'][text_key], keyboard_for(button_keys))
  end

  def show_reply_keyboard(text, keyboard)
    send_message(
      text: text,
      reply_markup:
        Telegram::Bot::Types::ReplyKeyboardMarkup.new(
          keyboard: keyboard,
          one_time_keyboard: true,
          resize_keyboard: true
        )
    )
  end

  def keyboard_for(button_keys)
    button_keys.map { |button_key| [texts['buttons'][button_key]] }
  end

  def build_reply_keyboard(button_keys)
    Telegram::Bot::Types::ReplyKeyboardMarkup.new(
      keyboard: keyboard_for(button_keys),
      one_time_keyboard: true,
      resize_keyboard: true
    )
  end

  def create_issue
    text = message.caption.gsub(%r{/add_issue}, '').strip
    newspaper_title, issue_number, date_str, version = text.split('_')
    newspaper = Newspaper.search(newspaper_title) if newspaper_title
    return unless [newspaper, issue_number, date_str].all?

    newspaper.newspaper_issues.create(
      telegram_file_id: message.document.file_id,
      file_name: message.document.file_name,
      number: issue_number[/\d+/].to_i,
      date: Date.parse(date_str),
      version: version.to_s[/\d+/].presence || 1
    )
  rescue Date::Error
    send_text_message(texts['errors']['cant_parse_date'])
    nil
  end

  def user
    @user ||= User.where(telegram_id: message.from.id).first || User.create!(telegram_id: message.from.id)
  end

  def send_text_message(text)
    send_message(text: text)
  end

  def send_document(params)
    default_message_params = { chat_id: message.chat.id, parse_mode: 'HTML' }
    api.send_document(default_message_params.merge(params))
  end

  def send_message(params)
    default_message_params = { chat_id: message.chat.id, parse_mode: 'HTML', disable_web_page_preview: true }
    api.send_message(default_message_params.merge(params))
  end

  def texts
    @texts ||= YAML.load_file('app/data/texts.yml')
  end
end
