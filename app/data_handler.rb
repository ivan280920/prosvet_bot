# frozen_string_literal: true

require_relative 'message_handler'

module DataHandler
  module_function

  def call(data)
    return unless data

    Telegram::Bot::Client.run(ENV['TELEGRAM_BOT_TOKEN']) do |bot|
      update = Telegram::Bot::Types::Update.new(data)
      MessageHandler.new(update.current_message, bot.api).call
    end
  end
end
