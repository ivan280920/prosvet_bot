# frozen_string_literal: true

class NewspaperIssue
  include Dynamoid::Document

  field :telegram_file_id, :string
  field :number, :integer
  field :file_name, :string
  field :date, :date
  field :version, :integer

  belongs_to :newspaper
end
