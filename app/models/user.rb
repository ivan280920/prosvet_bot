# frozen_string_literal: true

class User
  include Dynamoid::Document

  table key: :telegram_id

  field :telegram_id, :integer
  field :region, :string
  field :used_bot_at, :datetime
end
