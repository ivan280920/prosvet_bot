# frozen_string_literal: true

class Newspaper
  include Dynamoid::Document

  field :title, :string
  field :search_title, :string
  field :active, :boolean, default: true
  field :locations, :array, of: :string
  field :description, :string

  has_many :newspaper_issues

  def self.search(newspaper_title)
    where(search_title: newspaper_title.downcase.squeeze(' ').strip).first
  end

  def self.data_for_region(region)
    newspapers = where(active: true, 'locations.contains': region)
    data = newspapers.map do |newspaper|
      issue = newspaper.find_actual_issue
      next unless issue

      { newspaper: newspaper, issue: issue }
    end

    data.compact
  end

  def find_actual_issue
    newspaper_issues.max_by { |i| [i.date, i.version] }
  end
end
