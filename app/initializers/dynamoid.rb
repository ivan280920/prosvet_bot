# frozen_string_literal: true

Dynamoid.config.endpoint = 'http://dynamodb:8000' if ENV['AWS_SAM_LOCAL']
Dynamoid.config.namespace = 'prosvet_bot'
