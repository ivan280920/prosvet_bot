# frozen_string_literal: true

require_relative '../../app'

module Seeds
  module_function

  def call
    creaate_tables

    data.map do |newspaper|
      params = newspaper.merge(search_title: newspaper['title'].downcase)
      Newspaper.search(params['title'])&.update_attributes(params) ||
        Newspaper.create(params)
    end
  end

  def creaate_tables
    User.create_table
    Newspaper.create_table
    NewspaperIssue.create_table
  end

  def data
    YAML.load_file('app/data/seeds.yml')
  end
end
