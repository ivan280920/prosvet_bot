### Deploy
- install `docker` and `aws-sam-cli` (https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/serverless-sam-cli-install.html)
- download and deploy `telegram-bot-ruby-lambda-layer`; replace layer's arn in `template.yml`
- `sam build`
- `sam deploy --guided` (for the first time, later tou can run `sam deploy`)


### Local
- install `docker` and `aws-sam-cli` (https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/serverless-sam-cli-install.html)
- `docker network create lambda-local`
- `docker run -d -v "$PWD":/dynamodb_local_db --network lambda-local -p 8000:8000 --name dynamodb amazon/dynamodb-local`
- `sam build`
- `cp env.example.json env.json` and put there `TELEGRAM_BOT_TOKEN` of some bot for testing purposes
- download and run `ngrok` (`./ngrok http 3000`)
- `ruby app/scripts/set_bot_webhook.rb`
- `sam local start-api --debug --env-vars env.json --docker-network lambda-local`

Now you can test bot as usual bot via Telegram. Run `sam build` each time you change your code (no need to restart api).
