# frozen_string_literal: true

require_relative 'app'
require_relative 'app/scripts/seeds'

def lambda_handler(event:, context:) # rubocop:disable Lint/UnusedMethodArgument
  data = get_data(event)
  puts "data: #{data}"

  DataHandler.call(data)

  { statusCode: 200, body: { status: :ok }.to_json }
end

def get_data(event)
  body = event['body']
  body.is_a?(Hash) ? body : JSON.parse(body)
end
